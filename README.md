## SmartBreeze Window Mover

**SmartBreeze** is an automated window opening and closing device. We have observed that e.g. windows in bath or cellar rooms often remain opened even in winter. The building looses energy when a temperature difference indoor and outdoor is large. SmartBreeze is automatically opening when a button is pressed and closing after a given time period. Sensors could be extended to measure temperature differences and trigger open and closing cycles, controlled by an Arduino controller.

Parts are built with alloy and 3d printed. A left / right thread pulls the window. 

* compact
* lightweight
* low cost
* DIY
* open source

![Smarbreeze_final_small](https://git.fairkom.net/FHV/smartbreeze/-/wikis/uploads/4beba8e83da593b9c9a9aaed044c187d/Smarbreeze_final_small.jpg)

[Emulation Video](https://git.fairkom.net/FHV/smartbreeze/-/blob/master/img/ZST_Fensteroeffner.mp4?expanded=true&viewer=rich)

[Mechanical Construction](https://git.fairkom.net/FHV/smartbreeze/-/wikis/Mechanical)

[Electrical Controller](https://git.fairkom.net/FHV/smartbreeze/-/wikis/Electrical)

[Code](https://git.fairkom.net/FHV/smartbreeze/-/wikis/Code)

[Material List](https://git.fairkom.net/FHV/smartbreeze/-/wikis/Material)

[Making of](https://git.fairkom.net/FHV/smartbreeze/-/wikis/Making_Of)

SmartBreeze is the result of the [Sustainability Innovation Course](https://ethify.org/content/sustainability-innovation) at [Vorarlberg University of Applied Sciences](https://fhv.at) March - May 2020. 

License: [CC-by-sa](https://creativecommons.org/licenses/by-sa/4.0/) nico & maxi & pfeffi & georgi & [rasos](https://roland.alton.at)